<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<aside>
	<div id="end-side">
		<div class="inner wrapper">
			<div class="aside-nav fleft">
				<ul class="list1">
					<li><a href="#">Trang chủ</a></li>
					<li><a href="#">Sản phẩm mới</a></li>
					<li><a href="#">Sản phẩm hot</a></li>
					<li><a href="#">Tạo tài khoản</a></li>
					<li><a href="#">Liên hệ</a></li>
				</ul>
				<ul class="list1 alt">
					<li><a href="#">Có gì mới?</a></li>
					<li><a href="#">Liên hệ</a></li>
					<li><a href="#">Đánh giá</a></li>
					<li><a href="shopcart">Giỏ hàng</a></li>
					<li><a href="#">Thanh toán</a></li>
				</ul>
				<ul class="list1 alt last">
					<li><a href="#">Sản phẩm</a></li>
					<li><a href="#">Dịch vụ</a></li>
					<li><a href="#">Lịch sử mua hàng</a></li>
					<li><a href="#">Giảm giá</a></li>
					<li><a href="#">Bản đồ Site</a></li>
				</ul>
			</div>
			<div class="banner-fright">
				<!-- BOF- BANNER #5 display -->
				<div id="bannerFive">
					<a href="#"><img src="images/banner5.jpg" alt="Free Gift"
						title=" Free Gift " width="311" height="149"></a>
				</div>
				<!-- EOF- BANNER #5 display -->
			</div>
		</div>
	</div>
</aside>
<br>